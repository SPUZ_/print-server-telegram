from telegram.ext import *
import os
import subprocess
import hashlib

token = '1'
user = [1]

def hashing(file_name):
    return hashlib.md5(file_name.encode()).hexdigest() + ".pdf"

def start(update, context):
    print('nice')
    if update.effective_chat.id in user:
        context.bot.send_message(chat_id=update.effective_chat.id, text="Working")

def printer(update, context):
    print('something to print')
    if update.effective_chat.id in user:
        file = context.bot.getFile(update.message.document.file_id)
        context.bot.send_message(chat_id=update.effective_chat.id, text="K\nFilename: " + hashing(update.message.document.file_name))
        file.download(main_path + "storage/"+ hashing(update.message.document.file_name))
        subprocess.call(main_path + "PDFtoPrinter.exe " + "storage/"+ hashing(update.message.document.file_name))




if __name__ == '__main__':
    #storage = os.path.join(os.getcwd(), 'storage')
    #if 'storage' not in os.listdir(os.getcwd()):
    #    os.mkdir(storage)
    main_path = 'C:/Users/SPUZ_WIN-SRV/telegramprintserver/'
    #os.getcwd()

    updater = Updater(token, use_context=True)
    dispatcher = updater.dispatcher


    start_handler = CommandHandler('start', start)
    dispatcher.add_handler(start_handler)

    print_handler = MessageHandler(Filters.document.mime_type("application/pdf"), printer)
    dispatcher.add_handler(print_handler)

    updater.start_polling()
